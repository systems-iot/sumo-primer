# SUMO and TraCI Primer

This Sumo and TraCI primer accompanies the two preparation videos.

1. [Getting Started](https://vimeo.com/350015032)
2. [Controlling SUMO with TraCI and Python](https://vimeo.com/350015321)

## Installing

Clone the repository in `git`:

```
git clone https://gitlab.com/systems-iot/sumo-primer.git
cd sumo-primer
```

Create your virtualenv and activate it:

```
virtualenv venv
source venv/bin/activate
```

## Running

Make sure your `SUMO_HOME` environment variable is set to the path where your `sumo-gui` binary is found.

```
export SUMO_HOME=/path/to/sumo/installation
```

This current code assumes you are using SUMO installed from `brew` on MacOS.

Then run the script:

```
python controller.py
```
