
import sys
import os
from time import sleep

tools = os.path.join(os.environ['SUMO_HOME'], 'share/sumo/tools')
sys.path.append(tools)

import traci

# Assumes `sumo-gui` is on the path already.  If not, you will need to update it to include the fully qualified path to your sumo-gui binary
sumoBinary = "sumo-gui"
sumoCmd = [sumoBinary, "-c", "first_network.sumocfg", "--start", "--time-to-teleport", "100000000"]

traci.start(sumoCmd)

step = 1
while step < 10000:
    traci.simulationStep()
    speed = traci.vehicle.getSpeed("veh0")
    print(speed)
    
    current_road = traci.vehicle.getRoadID("veh0")
    
    if current_road == "top_east":
        traci.vehicle.setStop("veh0", "east_south", duration=30)
    
    
    step += 1
    sleep(0.5)
    
    
print("done!")